MAKEFILES += -rR --no-print-directory

SOURCE_FILES := regex.c

SOURCE_FILES += $(wildcard token/*.c)

LOCAL_CFLAGS := -Wall -std=c11

ifneq ($(CFLAGS),)
	LOCAL_CFLAGS += $(CFLAGS)
endif

ifeq ($(CC),)
	CC := gcc
endif

ifneq ($(DEBUG),)
	LOCAL_CFLAGS += -g -DDEBUG
endif

all:
	$(CC) $(LOCAL_CFLAGS) -I. $(SOURCE_FILES) -o regex

clean:
	rm regex
