#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "group.h"
#include "token.h"

int parse_group(void **data, token *tok, token **last, char *regexp, int i) {
    int len = strlen(regexp);
    if (i == (len-1)) {
        return -1;
    }

    int end = i+1;
    while (end < len && regexp[end] != ')')  {
        end++;
    }

    if (end == len) return -1;

    // (ab)
    // 0123
    int start = i+1;
    int relen = end-start;
    printf("%s\n", regexp);
    char *myptr = &regexp[start];
    printf("=== sub tokenize ===\n");
    token *mytok = tokenize(myptr, relen);
    printf("===  done done!  ===\n");

    group_token *g = malloc(sizeof(group_token));
    g->tomatch = mytok;
    *data = g;
    tok->test_match = match_group;
    tok->destroy = destroy_group;
    return relen+2;
}

int match_group(void *tokdata, char *cur_location, int chars_left, int chars_seen) {
    token *start = ((group_token*)tokdata)->tomatch;
    int total = 0;
    while (start != NULL && chars_left > 0) {
        printf("%d, %d\n", chars_left, chars_seen);
        int consumed = start->test_match(start->tokdata, cur_location, chars_left, chars_seen);
        if (consumed == -1) {
            // what a rip
            printf(";_;\n");
            return -1;
        }

        total += consumed;
        cur_location += sizeof(char) * consumed;
        chars_left -= consumed;
        chars_seen += consumed;
        start = start->next;

    }
    // didn't finish matching before we ran out of string
    if (start != NULL) return -1;
    return total;
}

void destroy_group(void *tokdata) {
    return;
}
