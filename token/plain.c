#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "plain.h"
#include "token.h"

/**
 * @param tokdata - pointer to a *_token struct
 * @param cur_location - pointer to a pointer that's the cur location in the string
 * @param chars_left - chars left in string
 * @return number of consumed chars, negative value for non-match
 */
int match_plain(void *tokdata, char* cur_location, int chars_left, int chars_seen) {
    if (chars_left == 0) {
        return -1;
    }
    int result = *cur_location == ((plain_token*)tokdata)->token;
    return result ? 1 : -1;
}

// nothing to clean up
void destroy_plain(void *tokdata) {}

int parse_plain(void **data, token *tok, token **last, char* regexp, int i) {
    *data = malloc(sizeof(plain_token));
    ((plain_token*)*data)->token = regexp[i];
    tok->test_match = &match_plain;
    tok->destroy = &destroy_plain;
    return 1;
}

