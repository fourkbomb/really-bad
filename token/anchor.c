#include "anchor.h"
#include "token.h"
#include <stdlib.h>
#include <stdio.h>

int match_anchor(void *tokdata, char* cur_location, int chars_left, int chars_seen) {
    switch (((anchor_token*)tokdata)->start) {
        case ANCHOR_START:
            if (chars_seen == 0) {
                return 0;
            }
            break;
        case ANCHOR_END:
            if (chars_left == 0) {
                return 0;
            }
            break;
    }
    return -1;

}

void destroy_anchor(void *tokdata) {}

int parse_anchor(void **data, token *tok, token **last, char* regexp, int i) {
    *data = malloc(sizeof(anchor_token));
    ((anchor_token*)*data)->start = (regexp[i] == '^' ? ANCHOR_START : ANCHOR_END);
    tok->test_match = &match_anchor;
    tok->destroy = &destroy_anchor;
    return 1;

}
