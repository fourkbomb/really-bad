#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "charclass.h"
#include "token.h"

int parse_charclass(void **data, token *tok, token **last, char *regexp, int i) {
    int maxidx = strlen(regexp) - 1;
    if (i >= maxidx) {
        return -1;
    }

    int end = i+1;
    while (end <= maxidx) {
        if (regexp[end] == ']') {
            if (end != (i+1)) {
                break; // found the end.
            }
        }
        end++;
    }

    if (end > maxidx) { // no end found
        return -1;
    }

    int j = i+1;
    int k = 0;
    char *valid = malloc(sizeof(char) * (end-j));
    for (; j < end; j++) {
        valid[k] = regexp[j];
        k++;
    }

    charclass_token *tokdata = malloc(sizeof(charclass_token));
    tokdata->valid = valid;
    tokdata->validlen = k;
    *data = tokdata;
    tok->test_match = &match_charclass;
    tok->destroy = &destroy_charclass;
    return k+2;
}

int match_charclass(void *tokdata, char *cur_location, int chars_left, int chars_seen) {
    int i;
    charclass_token *data = tokdata;
    for (i = 0; i < data->validlen; i++) {
        if (*cur_location == data->valid[i]) {
            return 1;
        }
    }
    return -1;
}

void destroy_charclass(void *tokdata) {
    free(((charclass_token*)tokdata)->valid);
}
