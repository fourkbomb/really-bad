#include "token.h"
#pragma once

enum {
    ANCHOR_START = 0,
    ANCHOR_END = 1,
};

typedef struct _anchor_token {
    int start;
} anchor_token;

int match_anchor(void *tokdata, char* cur_location, int chars_left, int chars_seen);

void destroy_anchor(void *tokdata);

int parse_anchor(void **data, token *tok, token **last, char* regexp, int i);
