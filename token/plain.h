#include "token.h"

#pragma once

typedef struct _plain_token {
    char token;
} plain_token;

int match_plain(void *tokdata, char* cur_location, int chars_left, int chars_seen);

void destroy_plain(void *tokdata);

int parse_plain(void **data, token *tok, token **last, char* regexp, int i);

