#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "token.h"
#include "token/anchor.h"
#include "token/charclass.h"
#include "token/group.h"
#include "token/plain.h"
#include "token/repeating.h"

#undef DEBUG

token *tokenize(char *regexp, int relen) {
    token *first = NULL;
    token *last = NULL;
    token *tok = NULL;

    assert(strlen(regexp) >= relen);
    void *data;
    int i, res;

    for (i = 0; i < relen; /**/) {
        tok = (token*) malloc(sizeof(token));
        int (*parse_func)(void**, token*, token**, char*, int) = NULL;
        switch (regexp[i]) {
            case '*':
            case '+':
            case '?':
                parse_func = &parse_repeating;
                tok->token_type = REPEATING;
                break;
            case '[':
                parse_func = &parse_charclass;
                tok->token_type = CHARCLASS;
                break;
            case '(':
                parse_func = &parse_group;
                tok->token_type = GROUP;
                break;
            case '^':
            case '$':
                parse_func = &parse_anchor;
                tok->token_type = ANCHOR;
                break;
            default:
                parse_func = &parse_plain;
                tok->token_type = PLAIN;
                break;
        }
        // parse, handle error/add to list
        assert(parse_func != NULL);
        res = parse_func(&data, tok, &last, regexp, i);

        if (last != NULL) {
            last->next = tok;
        }
        tok->last = last;
        if (tok->last == NULL) {
            first = tok;
        }

        if (res < 0) {
            goto error;
        } else {
            i += res;
        }

        tok->tokdata = data;
        tok->next = NULL;
        last = tok;

    }

    if (i == 0) {
        return NULL;
    }
    return first;
error: // cleanup tree
    free_all(first);
    return NULL;
}

void free_all(token *first) {
    token *cur = first;
    while (cur != NULL) {
        first = cur->next;
        free_one(cur);
        cur = first;
    }
}

void free_one(token *cur) {
    if (cur->tokdata != NULL) {
        cur->destroy(cur->tokdata);
        free(cur->tokdata);
    }
    free(cur);
}



int main(int argc, char *argv[]) {
    if (argc < 3) {
        printf("usage: %s <regexp> <test>\n", argv[0]);
        return 1;
    }
    char *str = argv[2];
    int len = strlen(str);
    int seen = 0;
    int res = -1;
    int toks = 0;
    token *first;
    token *tok = tokenize(argv[1], len);
    first = tok;
    while (tok != NULL) {

        res = tok->test_match(tok->tokdata, str, len, seen);
        if (tok == first) {
            while (res < 0 && len > 0) {
                len--;
                str += sizeof(char);
                seen += 1;
                res = tok->test_match(tok->tokdata, str, len, seen);
            }
        } else {
            res = tok->test_match(tok->tokdata, str, len, seen);
        }

        if (res >= 0) {
            len -= res;
            str += sizeof(char) * res;
            seen += res;
        } else {
            printf("regexp match failed! %d'th token, %d chars left, rest of string: '%s'\n", toks, len, str);
            return 1;
        }
        tok = tok->next;
        toks++;
    }
    printf("chars left unconsumed: %d\n", len);
    free_all(first);
    printf("nice!\n");

    return 0;
}
