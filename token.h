#pragma once

typedef struct _token {
    /**
     * test_match(tokdata, match_start, chars_left)
     *
     * check that the string slice from match_start is a valid match
     * for the token data provided
     *
     * @return number of chars consumed or number <0 for failure
     *
     * @param tokdata - *_token struct pointer
     * @param match_start - pointer to expected match start location
     * @param chars_left - number of characters until end of string
     * @param chars_seen - number of characters from beginning of string
     */
    int (*test_match)(void*, char*, int, int);
    /**
     * destroy(tokdata)
     *
     * clean up tokdata so it can be safely freed
     *
     * @param tokdata - *_token struct pointer
     */
    void (*destroy)(void*);
    /**
     * pointer to *_token struct
     */
    void *tokdata;
    struct _token *next;
    struct _token *last;
    int token_type;
} token;

enum token_type {
    PLAIN = 0,
    ANCHOR,
    GROUP,
    CHARCLASS,
    REPEATING,
    TOKEN_MAX
};

token *tokenize(char *regexp, int len);
void free_all(token *first);
void free_one(token *cur);

