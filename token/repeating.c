#include <stdio.h>
#include <stdlib.h>
#include "token.h"
#include "repeating.h"

// handles +?* and potentially one day {x,y}
int match_repeating(void *tokdata, char* cur_location, int chars_left, int chars_seen) {
    repeating_token *tok = tokdata;
    if (chars_left == 0 && tok->min == 0) {
        return 0;
    } else if (chars_left == 0) {
        return -1;
    }
    // max overflows to 2^(sizeof(int)-1)
    if (tok->max == 0) tok->max--;

    token *prev = tok->prev_token;

    unsigned int consumed = 0;
    char *loc = cur_location;

    while (consumed < tok->min) {
        int res = prev->test_match(tok->prev_token->tokdata, loc, chars_left, chars_seen);
        if (res != -1) {
            consumed += res;
            loc += sizeof(char) * res;
            chars_left -= res;
            chars_seen += res;
        } else {
            // the thing that's repeating didn't match - fail
            return -1;
        }
    }

    // we didn't manage to match the repeating token enough, rip :(
    if (consumed < tok->min) {
        return -1;
    }
    while (consumed < tok->max) {
        int res = prev->test_match(tok->prev_token->tokdata, loc, chars_left, chars_seen);
        if (res != -1) {
            consumed += res;
            loc += sizeof(char) * res;
            chars_left -= res;
            chars_seen += res;
            if (chars_left <= 0) {
                if (chars_left < 0)
                    fprintf(stderr, "WARNING: chars_left < 0!");
                // done - consumed enough but not too many chars
                return consumed;
            }
        } else {
            // done - consumed enough but not too many chars
            return consumed;
        }
    }
    // getting here means consumed >= tok->max - we succeeded
    return consumed;
}

void destroy_repeating(void *tokdata) {
    repeating_token *tok = tokdata;
    // we took tok->prev_token out of our tokens list, so
    // we have to clean it up here
    free_one(tok->prev_token);
}

int parse_repeating(void **data, token *tok, token **last, char *regexp, int i) {
    if (*last == NULL) return -1;
    repeating_token *t = malloc(sizeof(repeating_token));
    t->prev_token = *last;
    t->min = (regexp[i] == '+'); // 1 for '+', 0 for '?' and '*'
    t->max = (regexp[i] == '?'); // 1 for '?', 0 for '+' and '*'
    *data = t;

    tok->test_match = &match_repeating;
    tok->destroy = &destroy_repeating;
    *last = (*last)->last;
    return 1;
}
