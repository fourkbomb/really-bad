#pragma once

typedef struct _repeating_token {
    token *prev_token;
    unsigned int min;
    unsigned int max;
} repeating_token;

int parse_repeating(void **data, token *tok, token **last, char *regexp, int i);
int match_repeating(void *tokdata, char *cur_location, int chars_left, int chars_seen);
void destroy_repeating(void *tokdata);


