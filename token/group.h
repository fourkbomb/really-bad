#include "token.h"
#pragma once

typedef struct _group_token {
    token *tomatch;
} group_token;

int parse_group(void **data, token *tok, token **last, char *regexp, int i);

int match_group(void *tokdata, char *cur_location, int chars_left, int chars_seen);

void destroy_group(void *tokdata);
