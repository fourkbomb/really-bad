#include "token.h"
#pragma once

typedef struct _charclass_token {
    char *valid;
    int validlen;
} charclass_token;

int parse_charclass(void **data, token *tok, token **last, char *regexp, int i);

int match_charclass(void *tokdata, char *cur_location, int chars_left, int chars_seen);

void destroy_charclass(void *tokdata);
